/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rscharitas.model;

/**
 *
 * @author Altaire
 */
public class Tabel {
    private int no;
    private String nama;

    public int getNo() {
        return no;
    }

    public void setNo(int no) {
        this.no = no;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }
    
}
